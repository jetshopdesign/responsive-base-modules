![Logotype](http://responsive-base.jetshop.se/images/responsive-base/base-modules-logo.png)

# Responsive Base Modules



Repo for all untested modules created by designpartners.

This is not an environment for developing modules, consider this a code-bank for grabbing code and see other developers solutions.


________________

**Start the nodeserver: ``node Nodeserver.js``**

________________




## Prefix modules

Prefix modules with designpartner/customer signature:

``ebeling-module-name``

``hylte-module-name``



## How can I contribute? How do I add my modules?

1. Clone this repo to local machine.

1. Crate a branch locally.

1. Add one module and all it's dependencies. 

1. Start the Nodeserver and double-check so that module info looks good, and there are no errors: `node Nodeserver.js`

1. Add all files to git with `git add .` and commit as "Added module name-foobar".

1. Push to repo.

1. Visit [BitBucket](https://bitbucket.org/jetshopdesign/responsive-base-modules/pull-requests/new) and create a Pull request for the newly added branch.

1. Repeat for all modules you want to add, one Pull request per module. 



## How can I make changes to an existing module?

1. Clone this repo to local machine.

1. Crate a branch locally.

1. Edit module, commit as "Edited module name-foobar" and add an description on what have been changed and why.

1. Push to repo.

1. Visit [BitBucket](https://bitbucket.org/jetshopdesign/responsive-base-modules/pull-requests/new) and create a Pull request for the newly added branch.



## Edit the package.json file

Edit file package.json and add a correct module name, module description, author name & author email.

        {
          "name": "Ganilla Grace - Preheader",
          "description": "Adds a preheader and settings for standard appends.",
          "prototype": true,
          "author": {
            "name": "Christoffer Rydberg",
            "email": "christoffer.rydberg@ganillagrace.se"
          }
        }
        
        
>   Hint: Set "prototype" to false when the module feels somewhat generic.

