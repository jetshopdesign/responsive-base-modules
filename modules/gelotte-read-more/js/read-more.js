var readMore = {
    elementsToTrim: '.category-header-subtitle',    // Can be several classes or ID's, separate with comma ","
    readMoreText: '... (Läs mer)',
    readLessText: '(Läs mindre)',
    trimLength: 200,                                // Number of characters before trim
    trimMargin: 1.2,                                // Offset to not just trim a few words

    init: function () {
        readMore.trim();
        readMore.bindClick();
    },
    trim: function () {
        $(readMore.elementsToTrim).each(function () {
            var $this = $(this),
                trimLength = readMore.trimLength,
                trimMargin = readMore.trimMargin;

            if ($this.html().length > (trimLength * trimMargin)) {
                var text = $this.html(),
                    trimPoint = $this.html().indexOf(" ", trimLength),
                    newContent =
                        text.substring(0, trimPoint)    // Get the visible bit of the text
                        + '<span class="read-more">'
                        + text.substring(trimPoint)     // Get the rest of the text
                        + '</span><span class="toggle"><a href="#">'
                        + readMore.readMoreText
                        + '</a></span>';

                $this.html(newContent);
            }
            $this.addClass('trimmed');
        });
    },
    bindClick: function () {
        $('.toggle a').on('click', function (e) {
            e.preventDefault();
            var $this = $(this),
                textElement = $this.closest(readMore.elementsToTrim);

            textElement.toggleClass('toggled');

            if (textElement.hasClass('toggled')) {
                textElement.find('.toggle a').text(readMore.readLessText);
            } else {
                textElement.find('.toggle a').text(readMore.readMoreText);
            }
        });
    }
};

J.pages.addToQueue('category-page', readMore.init);