var ganillaPreheader = {
  settings: {
    moveLinks: false,
    linksToRight: false,
    cultureSelector: true
  },
  init: function () {
    J.switch.check();

    ganillaPreheader.translations();
    ganillaPreheader.addPreheader();

    if(ganillaPreheader.settings.cultureSelector && J.switch.width !== 'small') {
      ganillaPreheader.moveCultureSelectorToHeader();
    }
    
    if (ganillaPreheader.settings.moveLinks && J.switch.width !== 'small') {
      ganillaPreheader.movePageLinksToHeader();
    }
  },
  translations: function () {
    J.translations.push(
      {
        gpExample: {
          sv: "Exempel",
          en: "Example",
          da: "Example",
          nb: "Example",
          fi: "Example"
        },
      }
    );
  },
  addPreheader: function() {
    var html = J.views['ganilla-preheader/ganilla-preheader']();
    $('div#inner-wrapper').prepend(html);
  },
  moveCultureSelectorToHeader: function () {
    var cultureSelector = $('div.footer-box div.culture-selector-wrapper');
    var preHeaderWrapper = $('div.preheader__wrapper');

    // Append the culture selector to the opposite side of the links
    if (ganillaPreheader.settings.linksToRight) {
      preHeaderWrapper = preHeaderWrapper.find('div.preheader__left');
    } else {
      preHeaderWrapper = preHeaderWrapper.find('div.preheader__right');
    }

    cultureSelector.prependTo(preHeaderWrapper);
  },
  movePageLinksToHeader: function() {
    var linksWrapper = $('div.footer-box div.page-list-wrapper');
    var preHeaderWrapper = $('div.preheader__wrapper');

    if (ganillaPreheader.settings.linksToRight) {
      preHeaderWrapper = preHeaderWrapper.find('div.preheader__right');
    } else {
      preHeaderWrapper = preHeaderWrapper.find('div.preheader__left');
    }

    preHeaderWrapper.append(linksWrapper);
  }
};

J.pages.addToQueue("all-pages", ganillaPreheader.init);
