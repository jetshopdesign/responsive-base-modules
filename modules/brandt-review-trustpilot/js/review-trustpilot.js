var reviewTrustpilot = {
    name: "Truspilot - Reviews & ratings",
    version: "0.1.0",
    config: {
        //
        //  This info can be found in Trustpilots admin after a Trustbox have been created.
        //  Only one value is needed for template ID's, since we use only one Trustbox at a time.
        //
        domain: "foundation2.jetshop.se",                           // No http://
        businessUnitId: "57e4ee470000ff0005951584",
        templateIds: {
            reviewCollector: "56278e9abfbbba0bdcd568bc",            // For type "Review Collector"
            list: "539ad60defb9600b94d7df2c",                       // For type "List",
            horizontal: "5406e65db0d04a09e042d5fc",                 // For type "Horizontal"
            dropDown: "5418052cfbfb950d88702476"                    // For type "Dropdown"
        },
        locale: "sv-SE"
    },
    init: function () {
        reviewTrustpilot.injectScript();
        //reviewTrustpilot.renderReviewCollector();
        reviewTrustpilot.renderReviewList();
        //reviewTrustpilot.renderReviewHorizontal();
        //reviewTrustpilot.renderReviewDropDown();
    },
    injectScript: function () {
        (function e() {
            var e = document.createElement("script");
            e.type = "text/javascript", e.async = true, e.src = "//widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js";
            var t = document.getElementsByTagName("script")[0];
            t.parentNode.insertBefore(e, t)
        })();
    },
    renderReviewCollector: function (data) {
        var template = J.views['review-trustpilot/review-collector'];
        var html = template(reviewTrustpilot.config);
        $(".purchase-block-buy").append(html);
    },
    renderReviewList: function (data) {
        var template = J.views['review-trustpilot/review-list'];
        var html = template(reviewTrustpilot.config);
        $(".purchase-block-buy").append(html);
    },
    renderReviewHorizontal: function (data) {
        var template = J.views['review-trustpilot/review-horizontal'];
        var html = template(reviewTrustpilot.config);
        $(".product-tabs").before(html);
    },
    renderReviewDropDown: function (data) {
        var template = J.views['review-trustpilot/review-dropdown'];
        var html = template(reviewTrustpilot.config);
        $(".product-tabs").before(html);
    }
};

J.pages.addToQueue("product-page", reviewTrustpilot.init);
