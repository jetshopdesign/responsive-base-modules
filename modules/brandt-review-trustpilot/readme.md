Trustpilot

This module supports the following Trustboxes:
- List
- Review Collector
- Horizontal
- Dropdown

**Install:**

An account at Trustpilot is needed.

Visit https://businessapp.b2b.trustpilot.com/#/integrations/trustbox/library

Create a Trustbox, and replace the variables on config

Only one value is needed for template ID's, since we use only one Trustbox at a time.

Activate the corresponding render-function to display the Trustbox 
