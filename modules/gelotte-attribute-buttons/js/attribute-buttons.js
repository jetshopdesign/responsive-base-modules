var attributeButtons = {
    firstOptionVisible: false,                                  // Hides [Välj värde i droplista]
    originalSelectElement: '.product-attributes select:eq(0)', // First attribute select element

    init: function () {
        attributeButtons.appendPlaceholder();
        attributeButtons.build();
        attributeButtons.bindClick();
        if (!attributeButtons.firstOptionVisible) {
            attributeButtons.hideFirstAttribute();
        }
    },
    appendPlaceholder: function () {
        var $originalSelect = $(attributeButtons.originalSelectElement);

        // If product has attributes, append placeholder
        if ($originalSelect.length) {
            $originalSelect.after('<div id="main-attr-btns"></div>');
        }
    },
    build: function () {
        var $originalSelect = $(attributeButtons.originalSelectElement),
            data = [];

        $originalSelect.find('option').each(function () {
            var $this = $(this),
                temp = {};

            temp.name = $this.text();
            data.push(temp);
        });
        attributeButtons.render(data);
    },
    render: function (data) {
        // Render using template
        var template = J.views['attribute-buttons/attribute-buttons'];
        var html = template({attributes: data});
        $('#main-attr-btns').append(html);
    },
    bindClick: function () {
        var $originalSelect = $(attributeButtons.originalSelectElement),
            $button = $('#main-attr-btns').find('.main-attr-btn');

        $button.click(function () {
            var $this = $(this);

            // If not already selected
            if (!$this.hasClass('selected')) {
                $button.removeClass('selected');
                $this.addClass('selected');

                // Apply change to original select
                $originalSelect.val($this.text());
                $originalSelect.change();
            }
        });
    },
    hideFirstAttribute: function () {
        var $firstAttribute = $('#main-attr-btns').find('.main-attr-btn').first();
        $firstAttribute.hide();
    }
};

J.pages.addToQueue('product-page', attributeButtons.init);