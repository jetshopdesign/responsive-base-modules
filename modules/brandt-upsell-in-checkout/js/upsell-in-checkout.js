var upsellInCheckout = {

    settings: {
        categoryId: 158,
        numberOfProducts: 4,
        showOnlyOneRow: true
    },

    init: function () {
        J.translations.push(
            {
                buyButton: {
                    sv: "Köp",
                    en: "Buy",
                    da: "Köp",
                    nb: "Köp",
                    fi: "Köp"
                }
            },
            {
                infoButton: {
                    sv: "Läs mer",
                    en: "Read more",
                    da: "Läs mer",
                    nb: "Läs mer",
                    fi: "Läs mer"
                }
            }
        );
        J.api.category.getProducts(upsellInCheckout.renderCategory, upsellInCheckout.settings.categoryId, true, 5, upsellInCheckout.settings.categoryId, true, upsellInCheckout.settings.numberOfProducts, 1);
        $(window).on("dynamic-cart-closed", function () {
            if (J.checker.isCheckoutPage) {
                window.location.reload();
            }
        });
    },

    showOnlyOneRow: function () {
        if (upsellInCheckout.settings.showOnlyOneRow && J.checker.isCheckoutPage){
            var selector = "#upsell-in-checkout li.product-item";
            $(selector).show();
            var rows = getRows(selector);
            console.log(rows);
            $.each(rows[0], function(key, group) {
                $(this).show();
            });
            if (rows.length > 1) {
                $.each(rows[1], function() {
                    $(this).hide();
                });
            }
        }
    },

    renderCategory: function (data, callbackOptions) {
        data.categoryId = callbackOptions;
        var template = J.views['upsell-in-checkout/upsell-in-checkout'];
        var html = template(data);
        $(".checkout-main-wrapper").prepend('<div id="upsell-in-checkout-placeholder" class="list-category resetfloat"></div>');
        $("#upsell-in-checkout-placeholder").append(html);
    }
    
};

J.pages.addToQueue("checkout-page", upsellInCheckout.init);

$(window).resize(function () {
    upsellInCheckout.showOnlyOneRow();
});
