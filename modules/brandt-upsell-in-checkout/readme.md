Upsell in Checkout

Fetches products from specified category and displayes them in checkout.

**Settings**

    categoryId          Int     Category to fetch products from

    numberOfProducts    Int     Number of products to show from category
    
    showOnlyOneRow      Bool    Will hide row 2 and down on smaller devices