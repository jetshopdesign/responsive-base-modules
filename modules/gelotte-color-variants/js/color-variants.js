var colorVariants = {
    articleNumber: '',
    searchString: '',
    init: function () {
        colorVariants.getSearchString();
        colorVariants.attachPlaceholder();
        colorVariants.findVariants();
    },
    getSearchString: function () {
        colorVariants.articleNumber = $('.product-article-number-value').attr('content');

        colorVariants.searchString = colorVariants.articleNumber.split('_')[0];
    },
    findVariants: function () {
        J.api.search(colorVariants.callbackObject, colorVariants.articleNumber, true, 5, colorVariants.searchString, true, 10, 1);
    },
    callbackObject: {
        success: function (data, callbackOptions, textStatus, jqXHR) {
            colorVariants.renderProduct(data, callbackOptions);
        }
    },
    renderProduct: function (data, callbackOptions) {
        data = data.ProductItems;

        var template = J.views['color-variants/color-variants'];
        var html = template({variants: data});
        $("#color-variants-placeholder").append(html);
    },
    attachPlaceholder: function () {
        $('.product-description').append('<div id="color-variants-placeholder"></div>');
    }
};

J.pages.addToQueue("product-page", colorVariants.init);