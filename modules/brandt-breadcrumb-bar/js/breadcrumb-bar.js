//
//    Module: breadcrumb-bar
//
var breadcrumbBar = {
    products: [],
    item: {
        idExist: function (id) {
            for (var i = 0; i < breadcrumbBar.products.length; i++) {
                if (breadcrumbBar.products[i] === id) {
                    return true;
                }
            }
            return false;
        },
        remove: function (id) {
            var index = breadcrumbBar.products.indexOf(id);
            if (index > -1) {
                breadcrumbBar.products.splice(index, 1);
            }
        },
        add: function (id) {
            if (!breadcrumbBar.item.idExist(id)) {
                log("Adding " + id);
                breadcrumbBar.products.push(id);
                breadcrumbBar.localStorage.save();
            } else {
                log("id " + id + " exist!");
            }
        }
    },
    localStorage: {
        read: function () {
            var item = localStorage.getItem("breadcrumb-bar");
            if (item === "") {
                return false;
            } else {
                return JSON.parse(item);
            }
        },
        save: function () {
            localStorage.setItem("breadcrumb-bar", JSON.stringify(breadcrumbBar.products));
        }
    },
    render: function (data) {
        data.ProductItems = data.ProductItems.reverse();
        $("#footer").before("<div id='breadcrumb-bar-placeholder' class='category-list'></div>");
        var template = J.views['breadcrumb-bar/breadcrumb-bar'];
        var html = template(data);
        $("#breadcrumb-bar-placeholder").html(html);
    },
    init: function () {
        J.translations.push(
            {
                buyButton: {
                    sv: "Köp",
                    en: "Buy",
                    da: "Köp",
                    nb: "Köp",
                    fi: "Köp"
                }
            },
            {
                infoButton: {
                    sv: "Läs mer",
                    en: "Read more",
                    da: "Läs mer",
                    nb: "Läs mer",
                    fi: "Läs mer"
                }
            }
        );
        var data = breadcrumbBar.localStorage.read();
        if (data) {
            breadcrumbBar.products = data;
        }
        if (J.checker.isProductPage) {
            breadcrumbBar.item.add(J.data.productId);
            J.api.product.get(breadcrumbBar.render, null, true, 5, breadcrumbBar.products);
        }
    }
};

J.pages.addToQueue("all-pages", breadcrumbBar.init);
