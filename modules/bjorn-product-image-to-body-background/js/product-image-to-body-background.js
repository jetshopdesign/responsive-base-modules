/**
 * Product Image To Body Background
 * @type {{config: {excludedCategoryIDs: number[]}, init: productImageToBodyBackground.init}}
 */
var productImageToBodyBackground = {
    config: {
        // Excluded categories (where product img is not appropriate for body bg)
        excludedCategoryIDs: [
            /*266,
            303*/ // Example code, uncomment and add your real IDs
        ]
    },
    init: function () {
        J.translations.push(
            {
                previewButton: {
                    sv: "Förhandsvisning På/Av",
                    en: "Preview On/Off",
                    da: "Forhåndsvisning On/Off",
                    nb: "Forhåndsvisning På/Av",
                    fi: "Esikatselu Päälle/Pois"
                }
            }
        );

        var b = $('body'),
            bBg = b.css('background'),
            productClass = 'product-wrapper',
            btnClass = 'product-img-to-body-bg',
            btnActiveClass = 'product-is-previewed',
            onExcludedCategory = false;

        // Iterate excludedCategoryIDs to see if body has matching class, if so set onExcludedClass to true
        for (var i = 0; i < productImageToBodyBackground.config.excludedCategoryIDs.length; i++) {
            if ( (b.attr('class').indexOf('category-page-id-' + productImageToBodyBackground.config.excludedCategoryIDs[i].toString() + '')) > 0 ){
                onExcludedCategory = true;
                break;
            }
        }

        // Current category is NOT excluded, e.g. we want a preview button
        if (!onExcludedCategory) {

            // Add the 'product-img-to-body-bg' element to each product
            $('.' + productClass).append('<a href="#" class="' + btnClass + '" title="' + J.translate("previewButton") + '"><i class="fa fa-eye" aria-hidden="true"></i></a>');

            // On 'product-img-to-body-bg' click
            $('.' + btnClass).click(function (e) {

                // Prevent anchor's default behaviour
                e.preventDefault();

                var t = $(this),
                    tP = t.parent();

                // Button is active
                if (t.hasClass(btnActiveClass)) {

                    // Remove active class
                    t.removeClass(btnActiveClass);
                    // Revert to original body bg
                    b.css('background', bBg);

                    // Button is NOT active
                } else {

                    var tPImgSrc = tP.find('.product-image > a > img').attr('src'); // Find clicked button's product image

                    if (tPImgSrc) {

                        // Remove all active classes
                        $('.' + btnClass).removeClass(btnActiveClass);
                        // Add active class to clicked button
                        t.addClass(btnActiveClass);
                        // Add product's img to body
                        b.css('background', 'transparent url("' + tPImgSrc + '") repeat');

                    }
                }
            });
        }
    }
};
J.pages.addToQueue("category-page", productImageToBodyBackground.init);