Adds a "preview" button to each product in category- and startpage which, 
when clicked, toggles the product's image for body background.

NOTE: to add this function to startpage objects, add the following line to
``responsive-base-core.js``, furthest down in method ``startPageObjects``:
``productImageToBodyBackground.init();``

**Config JS**

``excludedCategoryIDs`` Don't add preview buttons to these categories

**Config SCSS**

Add

``.category-page-wrapper, .releware-recommendation-wrapper, #startpage_list, .list-category { ul { li { position: relative; } } }``

to ``_page-listproducts.scss`` for absolutely positioned "preview" button to display properly (or write your own css).