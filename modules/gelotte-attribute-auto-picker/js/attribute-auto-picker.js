var attributeAutoPicker = {
    init: function () {
        attributeAutoPicker.bindClick();
        attributeAutoPicker.autoPick();
    },
    bindClick: function () {
        var $attributeButton = $('.main-attr-btn');

        $attributeButton.on('click', function () {
            var $this = $(this),
                clickedAttribute = $this.children('span').text();

            setCookie("attribute", clickedAttribute);
        });
    },
    autoPick: function () {
        var chosenAttribute = getCookie("attribute"),
            $attributeChoices = $('.main-attr-btn span');

        $attributeChoices.each(function () {
            var $this = $(this),
                attribute = $this.text();

            if (attribute == chosenAttribute) {
                $this.click();
            }
        });
    }
};

J.pages.addToQueue("product-page", attributeAutoPicker.init);