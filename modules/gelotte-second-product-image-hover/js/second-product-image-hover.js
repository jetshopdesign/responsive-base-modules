var secondProductImageHover = {
    catID: J.data.categoryId,
    imageSize: "medium",
    init: function () {
        secondProductImageHover.appendImages();
        secondProductImageHover.bindHover();
    },
    appendImages: function () {
        var images = JetshopData.Images;

        // Loop products
        $('.product-outer-wrapper').each(function () {
            var $this = $(this),
                productId = $this.find('.product-wrapper h3 a').attr('data-productid'),
                secondImage = images[productId][1],
                prodUrl = $this.find('.product-image a').first().attr('href');

            // Get second image if exists
            if (typeof secondImage != 'undefined') {
                var imgSrc = '/pub_images/' + secondProductImageHover.imageSize + '/' + secondImage,
                    imgString = '<a href="' + prodUrl + '" class="secondary-image" style="display: none;"><img class="lazyimg" data-original="' + imgSrc + '" src="../images/grey.gif" /></a>';

                $this.find('.product-image').append(imgString);
            }
        });
    },
    bindHover: function () {
        var $images = $('.product-image');

        $images.on('mouseenter', function () {
            var $this = $(this),
                $secondImage = $this.find('a.secondary-image'),
                secondImageSrc = $secondImage.find('img').attr('data-original');

            $secondImage.find('img.lazyimg')
                .attr('src', secondImageSrc)    // lazy load on hover
                .removeClass('lazyimg');        // prevent multiple lazy loads

            $secondImage.fadeIn('fast');
        });
        $images.on('mouseleave', function () {
            var $this = $(this),
                $image2 = $this.find('a.secondary-image');
            $image2.stop().fadeOut('fast');
        });
    }
};

J.pages.addToQueue('category-page', secondProductImageHover.init);