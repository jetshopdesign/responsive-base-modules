
//
//  GET /modules
//
exports.index = function (req, res, next) {
    var path = require("path");
    var allModules = createFileList("modules");
    var activeModules = getActiveModules();
    modules = createModuleList(allModules, activeModules);
    res.render('modules', {title: "Responsive Base Modules", modules: modules, activeModules: activeModules});
};

exports.create = function (req, res, next) {
    var fs = require("fs");
    var moduleName = req.body.name;
    var modulesFolderPath = "modules/";
    var path = modulesFolderPath + moduleName;

    var mkdirSync = function (path) {
        try {
            fs.mkdirSync(path);
        } catch (e) {
            console.log(e);
            if (e.code != 'EEXIST') throw e;
            res.send(JSON.stringify({"status": "Folder exists", "error": e}));
        }
    };

    var writeFile = function (path, name, extension, content) {
        fs.writeFile(path + "/" + name + "." + extension, content, function (err) {
            if (err) {
                return console.log(err);
            }
        });
    };

    var packageJsonContent = function (moduleName) {
        var str = '';
        str += '{\n';
        str += '    "name": "' + moduleName + '",\n';
        str += '    "version": "0.1.0",\n';
        str += '    "prototype": true,\n';
        str += '    "description": "Add description here",\n';
        str += '    "author": {\n';
        str += '        "name": "",\n';
        str += '        "email": ""\n';
        str += '    }\n';
        str += '}\n';
        return str;
    };

    mkdirSync(path);
    mkdirSync(path + "/js");
    writeFile(path + "/js", moduleName, "js", "");
    mkdirSync(path + "/scss");
    writeFile(path + "/scss", "_" + moduleName, "scss", "");
    mkdirSync(path + "/views");
    writeFile(path + "/views", moduleName, "hbs", "");
    writeFile(path + "/", "readme", "md", "Add content here");
    writeFile(path + "/", "package", "json", packageJsonContent(moduleName));

    res.end(JSON.stringify({"success": "Folder created", "status": 200}));
};

exports.export = function (req, res, next) {
    var fs = require("fs");
    var Handlebars = require("handlebars");
    var sass = require("node-sass");
    var zip = require("zip-folder");

    var selectedModule = req.body.selected;
    console.log("Exporting module: " + selectedModule);

    //  Config
    var config = {
        folderPrefix: "standalone-"
    };
    //  Object to hold module information
    var obj = {
        name: selectedModule,
        views: [],
        scripts: [],
        scss: [],
        readme: ""
    };
    //  Strings to hold the output data
    var templates = "";
    var jsFiles = "";
    var sassFiles = "";
    var cssFile = "";

    // Setup temp-folder to hold all files
    setupTempFolder(config.folderPrefix + selectedModule);

    //  Create a file list
    var fileList = createFileList("core/modules/" + selectedModule);
    for (var key in fileList.children) {
        if (fileList.children[key].name == "views") {
            for (var file in fileList.children[key].children) {
                obj.views.push({
                    path: fileList.children[key].children[file].path,
                    name: fileList.children[key].children[file].name
                });
            }
        }
        if (fileList.children[key].name == "js") {
            for (var file in fileList.children[key].children) {
                //obj.scripts.push(fileList.children[key].children[file].path);
                obj.scripts.push({
                    path: fileList.children[key].children[file].path,
                    name: fileList.children[key].children[file].name
                });
            }
        }
        if (fileList.children[key].name == "scss") {
            for (var file in fileList.children[key].children) {
                obj.scss.push(fileList.children[key].children[file].path);
            }
        }
        if (fileList.children[key].name == "readme.md") {
            obj.readme = fileList.children[key].path;
        }
    }

    //  Pre-compile views
    var templateItemEnd = ');';
    if (obj.views.length > 0) {
        for (var key in obj.views) {
            var templateItemStart = '\n\nthis["J"]["views"]["' + obj.name + '/' + obj.views[key]["name"].replace(".hbs", "") + '"] = Handlebars.template(';
            var source = fs.readFileSync(obj.views[key]["path"], "utf8");
            var templateItem = Handlebars.precompile(source);
            templates += templateItemStart.concat(templateItem).concat(templateItemEnd);
        }
    }

    //  Combine all js
    if (obj.scripts.length > 0) {
        for (var key in obj.scripts) {
            var source = fs.readFileSync(obj.scripts[key]["path"], "utf8");
            jsFiles += "//\n// " + obj.scripts[key]["path"] + "\n// " + createTimestamp() + " \n//";
            jsFiles += "\r\n" + source;
        }
    }

    //  Add Handlebars templates
    jsFiles += templates;

    //  Write concatenated .js file
    fs.writeFile("tools/node/temp/" + config.folderPrefix + obj.name + "/" + obj.name + ".js", jsFiles, function (err) {
        if (err) {
            return console.log(err);
        }
    });

    //  Compile SASS
    if (obj.scss.length > 0) {
        //  Add all dependencies & settings
        var dependencies = "";
        dependencies += '$include-js-meta-styles: false;';
        dependencies += '$include-html-global-classes: false;';
        dependencies += '@import "../../../scss/foundation/global";';
        dependencies += '@import "../../../scss/foundation/functions";';
        dependencies += '@import "../../../scss/responsive-base/mixins";';
        dependencies += '@import "../../../scss/base_settings";';
        sassFiles += dependencies;

        //  Add all the content from modules 'scss' folder
        for (var key in obj.scss) {
            var source = fs.readFileSync(obj.scss[key], "utf8");
            sassFiles += source;
        }
        //  Output CSS file
        var result = sass.renderSync({
            includePaths: ["core/scss", "core/scss/foundation", "core/scss/responsive-base", "core/modules/" + obj.name + "/scss"],
            data: sassFiles,
            outputStyle: "nested",
            outFile: "tools/node/temp/" + obj.name + ".css",
            sourceMap: false
        });
        //console.log("Included SASS files:");
        //console.log(result.stats.includedFiles);

        //  Write CSS file to temp-folder
        fs.writeFile("tools/node/temp/" + config.folderPrefix + obj.name + "/" + obj.name + ".css", result.css, function (err) {
            if (err) {
                return console.log(err);
            }
        });
    }

    //  Copy readme.md to temp folder
    var readme = fs.readFileSync(obj.readme, "utf8");
    fs.writeFile("tools/node/temp/" + config.folderPrefix + obj.name + "/readme.md", readme, function (err) {
        if (err) {
            return console.log(err);
        }
    });

    //  Create a .zip file with the entire package
    zip("tools/node/temp/" + config.folderPrefix + obj.name, "tools/node/temp/" + config.folderPrefix + obj.name + ".zip", function(err) {
        if(err) {
            console.log( err);
        } else {
            console.log(config.folderPrefix + obj.name + ".zip created.");
        }
    });

    //  Copy .zip file to folder core/package
    fs.createReadStream("tools/node/temp/" + config.folderPrefix + obj.name + ".zip").pipe(fs.createWriteStream("core/package/" + config.folderPrefix + obj.name + ".zip"));

    //  Tell the browser we are done, display a download-link
    res.end(JSON.stringify({"success": config.folderPrefix + obj.name + ".zip", "status": 200}));

};

exports.download = function (req, res, next) {
    var fs = require("fs");
    var fileRequested = req.params[0];
    var fileData = fs.readFileSync("tools/node/temp/" + fileRequested);
    res.send(fileData);
    res.end();
};

exports.preview = function (req, res, next) {
    var fs = require("fs");
    var fileRequested = req.params[0];
    var fileData = fs.readFileSync(fileRequested);
    res.send(fileData);
    res.end();
};
