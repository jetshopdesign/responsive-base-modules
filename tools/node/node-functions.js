var fs = require("fs");
var path = require("path");
var child_process = require('child_process');
var markdown = require("markdown-js");

global.customHandlebarHelpers = {
    ifValue: function (conditional, state, args, options) {
        if (conditional == args) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    },
    ifValue2: function (conditional, options) {
        if (conditional == options.hash.equals) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    },
    contains: function (value, array, options) {
        array = ( array instanceof Array ) ? array : [array];
        return (array.indexOf(value) > -1) ? options.fn(this) : "";
    }
};

global.isInArray = function (arr, obj) {
    return (arr.indexOf(obj) != -1);
};

global.keyIsInArray = function (arr, obj) {
    return (arr.indexOf(obj["key"]) != -1);
};

global.getFtpPass = function () {
    var data;
    try {
        data = fs.readFileSync(".ftppass", 'utf8');
        return JSON.parse(fs.readFileSync(".ftppass", 'utf8'))
    } catch (e) {
        return false;
    }
};

global.getPackageJson = function () {
    var data;
    try {
        data = fs.readFileSync("package.json", 'utf8');
        return JSON.parse(fs.readFileSync("package.json", 'utf8'))
    } catch (e) {
        return false;
    }
};

global.getActiveModules = function () {
    return {
        active: []
    };
    //var data;
    //try {
    //    data = fs.readFileSync("./core/modules/modules.json", 'utf8');
    //    return JSON.parse(fs.readFileSync("./core/modules/modules.json", 'utf8'));
    //} catch (e) {
    //    return false;
    //}
};

global.getNpmVersion = function () {
    var output = child_process.execSync('npm -v');
    return output;
};

global.getGruntVersion = function () {
    var output = child_process.execSync('grunt --version');
    return output;
};

global.getGitVersion = function () {
    var output = child_process.execSync('git --version');
    return output;
};

global.createFileList = function (filename) {
    //http://stackoverflow.com/questions/11194287/
    var stats = fs.lstatSync(filename);
    var info = {
        path: filename,
        name: path.basename(filename),
        extension: path.extname(path.basename(filename)),
        id: stats.ino // inode is unique, right? Seems legit.
    };
    if (stats.isDirectory()) {
        info.isFolder = true;
        info.type = "folder";
        info.children =
            fs.readdirSync(filename, 'utf8').map(function (child) {
                childData = createFileList(filename + '/' + child);
                return childData;
            });
    }
    else if (stats.isFile()) {
        info.isFile = true;
        info.type = "file";
    }
    else if (stats.isSymbolicLink()) {
        info.isLink = true;
        info.type = "link";
    }
    else {
        info.isUnknown = true;
        info.type = "unknown";
    }
    return info;
};

global.createModuleList = function (allModules, activeModules) {
    for (var i = 0; i < allModules["children"].length; i++) {
        // Check if module is activated
        if (isInArray(activeModules.active, allModules["children"][i]["name"])) {
            allModules["children"][i]["active"] = true;
        }
        // Remove 'modules.json' from array
        if (allModules["children"][i]["name"] == "modules.json") {
            allModules["children"].splice(i, 1)
        }
        // Remove '.DS_Store' from array
        if (allModules["children"][i]["name"] == ".DS_Store") {
            allModules["children"].splice(i, 1)
        }
        for (var j = 0; j < allModules["children"][i]["children"].length; j++) {
            // Get content of package.json
            if (allModules["children"][i]["children"][j]["name"] === "package.json") {
                try {
                    JSON.parse(fs.readFileSync(allModules["children"][i]["children"][j]["path"], 'utf8'));
                    allModules["children"][i]["package"] = true;
                    allModules["children"][i]["packageData"] = JSON.parse(fs.readFileSync(allModules["children"][i]["children"][j]["path"], 'utf8'));
                } catch (e) {
                    allModules["children"][i]["error"] = "JSON.parse error in file: " + allModules["children"][i]["path"] + "/package.json";
                }
            }
            // Get content of readme.md
            if (allModules["children"][i]["children"][j]["name"] === "readme.md") {
                allModules["children"][i]["readme"] = true;

                allModules["children"][i]["readmeContent"] = markdown.makeHtml(fs.readFileSync(allModules["children"][i]["children"][j]["path"], 'utf8'));
            }
        }
    }
    return allModules;
};

global.getStyleFile = function (filePath, whiteList) {
    var content = fs.readFileSync(filePath, 'utf-8');
    return parseStyleFile(content, whiteList);
};

global.parseStyleFile = function (data, whiteList) {
    var dataArray = [];
    var matchingWhiteList = [];
    var lines = data.split('\n');
    // Find real values, ignore comments & empty lines
    for (var line = 0; line < lines.length; line++) {
        var row = lines[line];
        var key = row.substring(0, row.indexOf(':'));
        var value = row.substring(row.indexOf(':'), row.length).trim();
        value = value.replace(/^:/, '');
        value = value.replace(/^ /, '');
        value = value.replace(/\;$/, '');
        if ((key.length > 0) && (key.search("//") != 0)) { //TODO this is bad, only return if its the first two chars after trim()
            dataArray.push({"line": line, "key": key, "value": value});
        }
    }
    // Create an array with all values matching white list
    for (var j in whiteList) {
        if (whiteList[j]["type"] === "group") {
            var obj = {};
            obj.label = whiteList[j]["label"];
            obj.type = whiteList[j]["type"];
            matchingWhiteList.push(obj);
        } else {
            for (var item = 0; item < dataArray.length; item++) {
                var name = dataArray[item]["key"].trim();
                if (name == whiteList[j]["key"]) {
                    var obj = dataArray[item];
                    obj.label = whiteList[j]["label"];
                    obj.type = whiteList[j]["type"];
                    matchingWhiteList.push(obj);
                }
            }
        }
    }
    return {
        "parsedFile": dataArray,
        "whiteList": whiteList,
        "matchingWhiteList": matchingWhiteList
    };
};

global.updateStyleFile = function (filePath, data, res) {
    var findLine = parseInt(data[0]["value"]);
    var findKey = data[1]["value"];
    var replaceValue = data[2]["value"];
    var newRow = findKey + ": " + replaceValue + ";\n";
    var match = false;

    var content = fs.readFileSync(filePath, 'utf-8');
    var lines = content.split('\n');
    for (var line = 0; line < lines.length; line++) {
        var row = lines[line];
        if (line === findLine) {
            // Found the correct line number
            var key = row.substring(0, row.indexOf(':')).trim();
            if (findKey == key) {
                row = row + "\n";
                match = row;
                break;
            } else {
                //TODO send error
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify({error: "Line number do not match. Check SCSS-file and try again."}));
            }
        }
    }

    if (match) {
        var result = content.replace(match, newRow);
        fs.writeFile(filePath, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({success: "Style saved."}));
    }
};

global.setupTempFolder = function (folderName) {
    var fs = require("fs-extra");
    fs.removeSync('tools/node/temp', function (err) {
        if (err) return console.error(err);
    });
    fs.mkdirsSync('tools/node/temp/' + folderName, function (err) {
        if (err) return console.error(err);
    })
};

global.createTimestamp = function () {
    return new Date();
};
